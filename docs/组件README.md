# Vue开发时候怎么运行 ？

```
npm i
npm run serve
```

# Vue怎么打包 ？

```
npm run build
```

# Vue 和 Node 版本

```
Vue version: 3.2.36
Node.js version: v18.17.0
```

# 创建上传文件目录快捷方式

```
php artisan storage:link
```

# Laravel怎么运行 ？

```
php artisan serve   
```

# PHP版本 和 Laravel版本

```
PHP 8.1.10 (cli) (built: Aug 30 2022 18:08:04) (NTS Visual C++ 2019 x64)
Copyright (c) The PHP Group
Zend Engine v4.1.10, Copyright (c) Zend Technologies
Laravel Framework 10.34.1

```

# Laravel Pint

Laravel Pint 是一款面向极简主义者的 PHP 代码风格固定工具。Pint 是建立在 PHP-CS-Fixer 基础上，使保持代码风格的整洁和一致变得简单。

Pint 会随着所有新的 Laravel 应用程序自动安装，所以你可以立即开始使用它。默认情况下，Pint 不需要任何配置，将通过遵循 Laravel
的观点性编码风格来修复你的代码风格问题。

### 运行 Pint

可以通过调用你项目中的 vendor/bin 目录下的 pint 二进制文件来指示 Pint 修复代码风格问题：

```
./vendor/bin/pint
```

你也可以在特定的文件或目录上运行 Pint：

```
./vendor/bin/pint app/Models
./vendor/bin/pint app/Models/User.php
```

Pint 将显示它所更新的所有文件的详细列表。 你可以在调用 Pint 时提供 -v 选项来查看更多 Pint 修改的细节。

```          
./vendor/bin/pint -v
```          

如果你只想 Pint 检查代码中风格是否有错误，而不实际更改文件，则可以使用 --test 选项：

```
./vendor/bin/pint --test
```

# laravel-response

laravel-response 主要用来统一 API 开发过程中「成功」、「失败」以及「异常」情况下的响应数据格式。
实现过程简单，在原有的 \Illuminate\Http\JsonResponse进行封装，使用时不需要有额外的心理负担。
遵循一定的规范，返回易于理解的 HTTP 状态码，并支持定义 ResponseCodeEnum 来满足不同场景下返回描述性的业务操作码。

laravel 9.x - 10.x

```
composer require jiannei/laravel-response "^5.0" -vvv
composer require jiannei/laravel-enum "^3.0" -vvv # 可选
```

```
Response::success();
Response::ok();// 无需返回 data，只返回 message 情形的快捷方法
Response::localize(200101);// 无需返回 data，message 根据响应码配置返回的快捷方法
Response::accepted();
Response::created();
Response::noContent();
```

```
Response::fail();
Response::errorBadRequest();
Response::errorUnauthorized();
Response::errorForbidden();
Response::errorNotFound();
Response::errorMethodNotAllowed();
Response::errorInternal();
```

Controller 以外抛出异常
可以使用 abort 辅助函数抛出 HttpException 异常

```
abort(500102,'登录失败');

// 返回数据

{
    "status": "fail",
    "code": 500102,
    "message": "登录失败",
    "data": {},
    "error": {}
}
```

其他异常
开启 debug（APP_DEBUG=true）

```
{
    "status": "error",
    "code": 404,
    "message": "Http not found",
    "data": {},
    "error": {
        "message": "",
        "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
        "file": "/home/vagrant/code/laravel-api-starter/vendor/laravel/framework/src/Illuminate/Routing/AbstractRouteCollection.php",
        "line": 43,
        "trace": [
            {
                "file": "/home/vagrant/code/laravel-api-starter/vendor/laravel/framework/src/Illuminate/Routing/RouteCollection.php",
                "line": 162,
                "function": "handleMatchedRoute",
                "class": "Illuminate\\Routing\\AbstractRouteCollection",
                "type": "->"
            },
            {
                "file": "/home/vagrant/code/laravel-api-starter/vendor/laravel/framework/src/Illuminate/Routing/Router.php",
                "line": 646,
                "function": "match",
                "class": "Illuminate\\Routing\\RouteCollection",
                "type": "->"
            }
        ]
    }
}
```

# MysqlHelper

MysqlHelper 是一个便捷的通过PHP导入和导出Mysql数据库表结构和数据的工具,可以快速实现mysql的数据库的导入和导出.

🌈使用文档

#### 1.实例化 常规方法
```
use zjkal\MysqlHelper;
$mysql = new MysqlHelper('root', 'root', 'testdatabase', '127.0.0.1', '3306', 'utf8mb4', 'wp_');
```
#### 2. 方式二: 实例化后,通过setConfig方法设置数据库配置
```
   $mysql = new MysqlHelper();
   $mysql->setConfig(['username' => 'root', 'password' => 'root', 'database' => 'testdatabase']);
   MysqlHelper针对常用的框架做了兼容,可以直接使用框架的数据库配置, 比如ThinkPHP框架或Laravel框架

$mysql = new MysqlHelper();
$config = config('database.connections.mysql');
$mysql->setConfig($config);
```
#### 3. 导出数据
   如果实例化时, 已经设置了表前缀,导出的表名可以不用带前缀
   //导出数据库(包含表结构和数据)

```
$mysql->exportSqlFile('test.sql');
//仅导出数据库表结构
$mysql->exportSqlFile('test.sql', false);
//导出指定表的结构和数据
$mysql->exportSqlFile('test.sql', true, ['table1', 'table2']);
```

#### 4. 导入数据
   sql文件中的表前缀需要使用__PREFIX__占位符代替
   如果实例化时,已经设置了表前缀,则可以不用传入第二个参数

```
//导入数据库
$mysql->importSqlFile('test.sql');

//导入数据库,并自动替换表前缀
$mysql->importSqlFile('test.sql', 'wp_');
```

# TimeHelper

是一个简单易用的PHP时间日期助手类库,可以快速实现常用的时间日期操作,比如获取指定时间的秒数,获取友好的时间格式,判断时间范围,计算两个时间相差值,返回N小时/天/星期/月/年前或者后的时间戳等等

1. 获取需要的秒数
   一般用于设置缓存时间,设置结束时间等

```
//返回到今天晚上零点之前的秒数
TimeHelper::secondEndToday();

//返回N分钟的秒数(默认为1分钟)
TimeHelper::secondMinute(5);

//返回N小时的秒数(默认为1小时)
TimeHelper::secondHour(2);

//返回N天的秒数(默认为1天)
TimeHelper::secondDay(10);

//返回N周的秒数(默认为1周)
TimeHelper::secondWeek(4);
```

#### 2. 返回友好的日期格式,比如N秒前,N分钟前,N小时前等等
   一般用于社交类平台,评论,论坛等

```
//一共2个参数:
//第1个参数传入字符串类型的时间或者时间戳都可以，
//第2个参数为语言(默认为中文,需要英文请传入en)
TimeHelper::toFriendly('2022-3-2 10:15:33');
//英文
TimeHelper::toFriendly(1646186290, 'en');
```

#### 3. 判断时间范围

```
//判断日期是否为今天
TimeHelper::isToday('2020-4-10 23:01:11');

//判断日期是否为本周
TimeHelper::isThisWeek('2020-5-1');

//判断日期是否为本月
TimeHelper::isThisMonth(1586451741);

//判断日期是否为今年
TimeHelper::isThisYear('Apr 11, 2020');

//判断指定时间是星期几,不传默认为当前时间. 返回值为1-7,1为星期一,7为星期日
TimeHelper::getWeekDay('2022-11-27');

//判断指定时间是否为平常日(周一到周五)
TimeHelper::isWeekday('2023-03-08');

//判断指定时间是否为周末(周六和周日)
TimeHelper::isWeekend(1586451741);
```

#### 4. 计算两个时间相差值
   如果只传入一个参数,则与当前时间比较

```
//计算两个日期相差天数
TimeHelper::diffDays('2022-4-10 23:01:11','Apr 11, 2020');

//计算两个日期相差周数
TimeHelper::diffWeeks('2022-4-10 23:01:11');

//计算两个日期相差月数
TimeHelper::diffMonths(1586451741,'Apr 11, 2020');

//计算两个日期相差年数
TimeHelper::diffYears('2022-4-10 23:01:11','Apr 11, 2020');

//比较两个时间的大小,如果第二个参数为空,则与当前时间比较
//第一个时间大于第二个时间则返回1,小于则返回-1,相等时则返回0
TimeHelper::compare('2022-4-10 23:01:11','Apr 11, 2020');
```

#### 5. 返回N小时/天/星期/月/年前或者后的时间戳
   只传入1个参数以当前时间计算,传入第2个参数则以该时间计算,传入第3个参数为true,则时间取整

```
//返回指定时间3分钟前0秒的时间戳
TimeHelper::beforeMinute(3,'2022-3-2 10:15:33',true);

//返回当前时间5分钟后的时间戳
TimeHelper::afterMinute(5);

//返回指定时间1小时前的时间戳(请注意此用法为php8之后的用法)
TimeHelper::beforeHour(datetime:'Apr 11, 2020');

//返回2小时后的时间戳
TimeHelper::afterHour(2);

//返回15天前0点的时间戳
TimeHelper::beforeDay(15,null,true);

//返回15天后的时间戳
TimeHelper::afterDay(15);

//返回指定时间2星期前的时间戳
TimeHelper::beforeWeek(2,'2022-4-10 23:01:11');

//返回指定时间10星期后的时间戳
TimeHelper::afterWeek(10,1646360133);

//返回指定时间1个月前的时间戳(请注意此用法为php8之后的用法)
TimeHelper::beforeMonth(datetime:1646360133);

//返回5个月后的时间戳
TimeHelper::afterMonth(5);

//返回指定时间3年前的时间戳
TimeHelper::beforeYear(3,'2022-7-11');

//返回2年后的时间戳
TimeHelper::afterYear(2);
```

#### 6.获取当前秒级/毫秒级/微秒级/纳秒级的时间戳
生成订单号或者与其他编程语言对接时可能会用到
```
//获取秒级的时间戳,可用time()代替
TimeHelper::getTimestamp();

//获取毫秒级的时间戳
TimeHelper::getMilliTimestamp();

//获取微秒级的时间戳
TimeHelper::getMicroTimestamp();

//获取纳秒级的时间戳
TimeHelper::getNanoTimestamp();
```
#### 7.日期转换

用于爬虫爬取网页或第三方程序对接时,时间格式不统一的转换
```
//将任意格式的时间转换为指定格式
//第一个参数是时间格式,与系统函数date()的格式保持一致
//第二个参数则是任意格式的时间日期,不传则默认为当前时间,可用系统函数date()代替
TimeHelper::format('Y-m-d H:i:s','May 3, 2022');

//判断一个字符串是否为时间戳,是返回true,否返回false
TimeHelper::isTimestamp(1646360133);

//将任意时间类型的字符串转为时间戳
TimeHelper::toTimestamp('Apr 11, 2020');
```
#### 8.平闰年相关
比原生方法使用起来更方便
```
//判断是否为闰年,是返回true,否返回false
TimeHelper::isLeapYear('2020-3-2 10:15:33');

//判断该日期的当年有多少天
TimeHelper::daysInYear(1646360133);

//判断该日期的当月有多少天
TimeHelper::daysInMonth('Apr 11, 2020');
```

#### 9.时区相关 🆕
主要提供获取不同时区的时间和转换不同时区的时间的方法

```
//将任意格式的时间或时间戳转换为指定时区的时间
//第一个参数为要转换的目标时区
//第二个参数为原时区,不传则默认为当前时区
//第三个参数为任意格式的时间或时间戳,不传则默认为当前时间
//第四个参数为时间格式,与系统函数date()的格式保持一致,不传则默认为Y-m-d H:i:s
TimeHelper::timezoneFormat('Europe/London', 'Asia/Shanghai', '2023-8-15 19:16:43', 'H:i:s');
//获得当前洛杉矶的时间
TimeHelper::timezoneFormat('America/Los_Angeles');
//将洛杉矶时间转换为伦敦时间
TimeHelper::timezoneFormat('Europe/London', 'America/Los_Angeles', 'Aug 15, 2023 10:15:33', 'H:i:s');
//将时间戳转换为伦敦时间
TimeHelper::timezoneFormat('Europe/London', null, 1692097543);
所支持的时区列表请参考时区列表
```

#### 10.国内节假日/工作日相关 🔥
专门针对国内的节假日进行判断,目前包含2020年-2024年的节假日数据,后续也会持续更新.
为了便于维护,另起了一个类ChinaHoliday,同样可以传入任意类型的时间格式或时间戳

```
use zjkal\ChinaHoliday;

//判断指定日期是否为国内的工作日
ChinaHoliday::isWorkday('2023-01-23');

//判断指定日期是否为国内的节假日
ChinaHoliday::isHoliday(1646360133);
特别说明: 所有时间的方法都可以传入任意格式的时间或者时间戳, 但是有一点请注意 m/d/y 或 d-m-y 格式的日期，如果分隔符是斜线（/），则使用美洲的 m/d/y 格式。如果分隔符是横杠（-）或者点（.），则使用欧洲的 d-m-y 格式。为了避免潜在的错误，您应该尽可能使用 YYYY-MM-DD 格式或其他格式.
```

# EasyWeChat
📦 一个 PHP 微信开发 SDK，开源 SaaS 平台提供商 微擎 旗下开源产品。
使用示例
基本使用（以公众号服务端为例）:
[开发文档](https://easywechat.com)
```
<?php

use EasyWeChat\OfficialAccount\Application;

$config = [
    'app_id' => 'wx3cf0f39249eb0exxx',
    'secret' => 'f1c242f4f28f735d4687abb469072xxx',
    'aes_key' => 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFG',
    'token' => 'easywechat',
];
$app = new Application($config);
$app->getServer()->with(fn() => "您好！EasyWeChat！");
$response = $server->serve();
```
