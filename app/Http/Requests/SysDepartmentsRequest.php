<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * 部门管理 FormRequest
 */
class SysDepartmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'required',
            'label' => [
                'required',
                'string',
                'max:50',
                Rule::unique('sys_departments', 'label')->ignore(request('id')),
            ],
            'remark' => '',
            'sort' => 'integer',
            'status' => 'nullable|boolean',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'parent_id.required' => '父ID是必填项。',
            'label.required' => '名称是必填项。',
            'label.max' => '名称不能超过50个字符。',
            'label.unique' => '名称已存在。',
            'remark.required' => '备注是必填项。',
            'remark.max' => '备注不能超过50个字符。',
            'sort.required' => '排序是必填项。',
            'sort.integer' => '排序必须是整数。',
            'status.boolean' => '是否有效必须是布尔值。',
        ];
    }
}
