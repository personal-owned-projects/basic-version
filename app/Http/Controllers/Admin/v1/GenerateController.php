<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;
use App\Models\Sys\SysMenu;
use App\Models\Sys\SysMenuMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jiannei\Response\Laravel\Support\Facades\Response;

/**
 * 代码生成器 - 控制器
 */
class GenerateController extends Controller
{
    /**
     * 列表
     * index function
     *
     * @desc Display a listing of the resource.
     * @desc index： GET /re
     *
     * @param paging 是否分页，默认true
     * @param limit 显示条数
     * @param page 当前页
     * @param sort 排序参数
     * @param sortOrder 排序方式
     * @return void
     */
    public function index(Request $request)
    {
        // 请求参数
        $param = $request->all();
        // 查询SQL
        $sql = 'SHOW TABLE STATUS WHERE 1';
        // 表名称
        $name = getter($param, 'name');
        if ($name) {
            $sql .= " AND NAME like \"%{$name}%\" ";
        }
        // 表描述
        $comment = getter($param, 'comment');
        if ($comment) {
            $sql .= " AND COMMENT like \"%{$comment}%\" ";
        }
        $list = DB::select($sql);
        $list = json_decode(json_encode($list), true);
        $list = array_map('array_change_key_case', $list);

        return Response::success([
            'total' => count($list), // 总页数
            'page' => 1, // 当前页
            'pageSize' => 1, // 总页数
            'rows' => $list, // 数据
        ]);
    }

    /**
     * 创建保存
     * store function
     *
     * @desc Store a newly created resource in storage.
     * @desc store： POST /re
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestData = $request->all(['name', 'type', 'comment']);
        if (is_empty($requestData['name'])) {
            return Response::fail('请选择数据表');
        }

        $tableName = $this->getTableName($requestData['name']);
        $moduleName = $this->getModuleName($tableName);
        $comment = $this->getComment($requestData['comment']);
        $build = $this->getColumnList($requestData['name']);

        // 判断类型生成对应文件
        if ($requestData['type'] == 'model') {
            // 生成model
            $this->generateModel($tableName, $moduleName, $comment, $build);
        } elseif ($requestData['type'] == 'controller') {
            // 生成controller
            $this->generateController($tableName, $moduleName, $comment, $build);
        } elseif ($requestData['type'] == 'formRequest') {
            // 生成formRequest
            $this->generateFormRequest($tableName, $moduleName, $comment, $build);
        } elseif ($requestData['type'] == 'vue-router') {
            // 生成vue路由
            $this->generateVueRouter($tableName, $moduleName, $comment, $build);
        } elseif ($requestData['type'] == 'vue-page') {
            // 生成vue页面
            $this->generateVuePage($tableName, $moduleName, $comment, $build);
        } elseif ($requestData['type'] == 'menu') {
            $res = $this->createMenu($moduleName, $comment);
            if (!$res) {
                return Response::fail('生成菜单失败');
            }
        } elseif ($requestData['type'] == 'all') {
            $this->generateModel($tableName, $moduleName, $comment, $build);
            $this->generateController($tableName, $moduleName, $comment, $build);
            $this->generateFormRequest($tableName, $moduleName, $comment, $build);
            $this->generateVueRouter($tableName, $moduleName, $comment, $build);
            $this->generateVuePage($tableName, $moduleName, $comment, $build);

            $res = $this->createMenu($moduleName, $comment);
            if (!$res) {
                return Response::fail('生成菜单失败');
            }
        }

        return Response::ok(sprintf('本次共生成【%d】个模块', 1));
    }

    /**
     * 创建菜单
     * @param $moduleName 模块名称
     * @param $comment 表注释
     * @return void
     */
    public function createMenu($moduleName, $comment)
    {
        $id = DB::transaction(function () use ($moduleName, $comment) {
            // 查询菜单是否存在存在删除
            $models = SysMenu::query();
            $count = $models->where('name', strtolower($moduleName))->count();
            if($count){
                $models->where('name', strtolower($moduleName))->delete();
            }
            // 添加数据
            $data = [
                'parent_id' => 0,
                'name' => strtolower($moduleName),
                'path' => '/customize/' . strtolower($moduleName),
                'redirect' => '',
                'component' => 'customize/' . strtolower($moduleName),
                'active' => 0,
                'sort' => '99',
            ];
            $createdModel = $models->create($data);
            // 保存Meta
            $metaData = [
                'menu_id' => $createdModel->id,
                'color' => '',
                'hidden' => 0,
                'hidden_breadcrumb' => 0,
                'title' => $comment,
                'type' => 'menu',
                'affix' => 0,
                'fullpage' => 0
            ];
            return SysMenuMeta::insert($metaData);
        });
        return $id;
    }


    /**
     * 生成模型
     *
     * @param string $tableName 数据表名称
     * @param string $moduleName 模型名称
     * @param string $comment 数据表注释
     * @param array $build 数据表结构
     * @param string $author 作者
     * @return mixed|true
     */
    public function generateModel(string $tableName, string $moduleName, string $comment, array $build, string $author = 'zhx')
    {
        $moduleImage = false;
        foreach ($build as &$val) {
            // 图片字段处理
            if (strpos($val['columnName'], 'cover') !== false ||
                strpos($val['columnName'], 'avatar') !== false ||
                strpos($val['columnName'], 'image') !== false ||
                strpos($val['columnName'], 'logo') !== false ||
                strpos($val['columnName'], 'pic') !== false) {
                $val['columnImage'] = true;
                $moduleImage = true;
            }
        }
        // 参数
        $param = [
            'author' => $author,
            'since' => date('Y/m/d', time()),
            'moduleName' => $moduleName,
            'moduleTitle' => $comment,
            'tableName' => $tableName,
            'columnList' => $build,
            'moduleImage' => $moduleImage,
            'columnName' => array_column($build, 'columnName'),
        ];

        // 存储目录
        $FILE_PATH = app_path() . '/Models';
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/{$moduleName}Model.php";
        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/model.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, '<?php ' . $out);
        // 关闭
        fclose($f);
    }

    /**
     * 生成 formRequest验证类
     *
     * @param string $tableName 数据表名称
     * @param string $moduleName 模型名称
     * @param string $comment 数据表注释
     * @param array $build 数据表结构
     * @param string $author 作者
     * @return void
     */
    public function generateFormRequest(string $tableName, string $moduleName, string $comment, array $build, string $author = 'zhx')
    {
        $moduleImage = false;
        foreach ($build as &$val) {
            // 图片字段处理
            if (strpos($val['columnName'], 'cover') !== false ||
                strpos($val['columnName'], 'avatar') !== false ||
                strpos($val['columnName'], 'image') !== false ||
                strpos($val['columnName'], 'logo') !== false ||
                strpos($val['columnName'], 'pic') !== false) {
                $val['columnImage'] = true;
                $moduleImage = true;
            }
        }
        // 参数
        $param = [
            'author' => $author,
            'since' => date('Y/m/d', time()),
            'moduleName' => $moduleName,
            'moduleTitle' => $comment,
            'tableName' => $tableName,
            'columnList' => $build,
            'moduleImage' => $moduleImage,
            'columnName' => array_column($build, 'columnName'),
        ];

        // 存储目录
        $FILE_PATH = app_path() . '/Http/Requests';
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/{$moduleName}Request.php";
        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/formRequest.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, '<?php ' . $out);
        // 关闭
        fclose($f);
    }

    /**
     * 生成 controller 控制器
     *
     * @param string $tableName 数据表名称
     * @param string $moduleName 模型名称
     * @param string $comment 数据表注释
     * @param array $build 数据表结构
     * @param string $author 作者
     * @return void
     */
    public function generateController(string $tableName, string $moduleName, string $comment, array $build, string $author = 'zhx')
    {
        $moduleImage = false;
        foreach ($build as &$val) {
            // 图片字段处理
            if (strpos($val['columnName'], 'cover') !== false ||
                strpos($val['columnName'], 'avatar') !== false ||
                strpos($val['columnName'], 'image') !== false ||
                strpos($val['columnName'], 'logo') !== false ||
                strpos($val['columnName'], 'pic') !== false) {
                $val['columnImage'] = true;
                $moduleImage = true;
            }
        }
        // 参数
        $param = [
            'author' => $author,
            'since' => date('Y/m/d', time()),
            'moduleName' => $moduleName,
            'moduleTitle' => $comment,
            'tableName' => $tableName,
            'columnList' => $build,
            'moduleImage' => $moduleImage,
            'columnName' => array_column($build, 'columnName'),
        ];

        // 存储目录
        $FILE_PATH = app_path() . '/Http/Controllers/Admin';
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/{$moduleName}Controller.php";
        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/controller.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, '<?php ' . $out);
        // 关闭
        fclose($f);
    }

    /**
     * 生成 vue router 页面
     *
     * @param string $tableName 数据表名称
     * @param string $moduleName 模型名称
     * @param string $comment 数据表注释
     * @param array $build 数据表结构
     * @param string $author 作者
     * @return void
     */
    public function generateVueRouter(string $tableName, string $moduleName, string $comment, array $build, string $author = 'zhx')
    {
        $moduleImage = false;
        foreach ($build as &$val) {
            // 图片字段处理
            if (strpos($val['columnName'], 'cover') !== false ||
                strpos($val['columnName'], 'avatar') !== false ||
                strpos($val['columnName'], 'image') !== false ||
                strpos($val['columnName'], 'logo') !== false ||
                strpos($val['columnName'], 'pic') !== false) {
                $val['columnImage'] = true;
                $moduleImage = true;
            }
        }
        // 参数
        $param = [
            'author' => $author,
            'since' => date('Y/m/d', time()),
            'moduleName' => strtolower($moduleName),
            'moduleTitle' => $comment,
            'tableName' => $tableName,
            'columnList' => $build,
            'moduleImage' => $moduleImage,
            'columnName' => array_column($build, 'columnName'),
        ];

        // 存储目录
        $FILE_PATH = base_path() . '/scui/src/api/model';
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/{$param['moduleName']}.js";

        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/vue/api.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, $out);
        // 关闭
        fclose($f);

    }

    /**
     * 生成 vue page 页面
     *
     * @param string $tableName 数据表名称
     * @param string $moduleName 模型名称
     * @param string $comment 数据表注释
     * @param array $build 数据表结构
     * @param string $author 作者
     * @return void
     */
    public function generateVuePage(string $tableName, string $moduleName, string $comment, array $build, string $author = 'zhx')
    {
        $moduleImage = false;
        foreach ($build as &$val) {
            // 图片字段处理
            if (strpos($val['columnName'], 'cover') !== false ||
                strpos($val['columnName'], 'avatar') !== false ||
                strpos($val['columnName'], 'image') !== false ||
                strpos($val['columnName'], 'logo') !== false ||
                strpos($val['columnName'], 'pic') !== false) {
                $val['columnImage'] = true;
                $moduleImage = true;
            }
        }
        // 参数
        $param = [
            'author' => $author,
            'since' => date('Y/m/d', time()),
            'moduleName' => strtolower($moduleName),
            'moduleTitle' => $comment,
            'tableName' => $tableName,
            'columnList' => $build,
            'moduleImage' => $moduleImage,
            'columnName' => array_column($build, 'columnName'),
        ];

        // 存储目录
        $FILE_PATH = base_path() . '/scui/src/views/customize/' . $param['moduleName'];
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/{$param['moduleName']}.vue";

        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/vue/index.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, $out);
        // 关闭
        fclose($f);


        // 存储目录
        $FILE_PATH = base_path() . '/scui/src/views/customize/' . $param['moduleName'];
        if (!is_dir($FILE_PATH)) {
            // 创建目录并赋予权限
            mkdir($FILE_PATH, 0777, true);
        }
        // 文件名
        $filename = $FILE_PATH . "/save.vue";

        // 拆解参数
        extract($param);
        // 开启缓冲区
        ob_start();
        // 引入模板文件
        require resource_path() . '/views/templates/vue/save.blade.php';
        // 获取缓冲区内容
        $out = ob_get_clean();
        // 打开文件
        $f = fopen($filename, 'w');
        // 写入内容
        fwrite($f, $out);
        // 关闭
        fclose($f);

    }


    /**
     * 获取数据库表名
     *
     * @return void
     */
    private function getTableName(string $name)
    {
        // +----------------------------------------------------------------------
        // | 数据表名称
        // +----------------------------------------------------------------------
        return str_replace(env('DB_PREFIX'), null, $name);
    }

    /**
     * 获取数据库表名
     *
     * @return void
     */
    private function getModuleName(string $name)
    {
        // +----------------------------------------------------------------------
        // | 模型名称
        // +----------------------------------------------------------------------
        return str_replace(' ', null, ucwords(strtolower(str_replace('_', ' ', $name))));
    }

    /**
     * 获取数据库表注释
     *
     * @return void
     */
    private function getComment(string $comment)
    {
        // +----------------------------------------------------------------------
        // | 数据表注释
        // +----------------------------------------------------------------------

        // 去除表描述中的`表`
        if (strpos($comment, '表') !== false) {
            $comment = str_replace('表', null, $comment);
        }
        // 去除表描述中的`管理`
        if (strpos($comment, '管理') !== false) {
            $comment = str_replace('管理', null, $comment);
        }

        return $comment;
    }

    /**
     * 生成字段列表
     *
     * @param  $tableName  数据表名
     * @return array
     *
     * @author 牧羊人
     *
     * @since 2020/11/12
     */
    private function getColumnList($tableName)
    {
        // 获取表列字段信息
        $columnList = DB::select("SELECT COLUMN_NAME,COLUMN_DEFAULT,DATA_TYPE,COLUMN_TYPE,COLUMN_COMMENT FROM information_schema.`COLUMNS` where TABLE_SCHEMA = '" . env('DB_DATABASE') . "' AND TABLE_NAME = '{$tableName}'");
        $columnList = json_decode(json_encode($columnList), true);
        $fields = [];
        if ($columnList) {
            foreach ($columnList as $val) {

                if (array_search($val['COLUMN_NAME'], ['', 'created_at', 'updated_at', 'deleted_at'])) {
                    break;
                }

                $column = [];
                // 列名称
                $column['columnName'] = $val['COLUMN_NAME'];
                // 列默认值
                $column['columnDefault'] = $val['COLUMN_DEFAULT'];
                // 数据类型
                $column['dataType'] = $val['DATA_TYPE'];
                // 列描述
                if (strpos($val['COLUMN_COMMENT'], '：') !== false) {
                    $item = explode('：', $val['COLUMN_COMMENT']);
                    $column['columnComment'] = $item[0];

                    // 拆解字段描述
                    $param = explode(' ', $item[1]);
                    $columnValue = [];
                    $columnValueList = [];
                    $typeList = ['', 'success', 'warning', 'danger', 'info', '', 'success', 'warning', 'danger', 'info', '', 'success', 'warning', 'danger', 'info'];
                    foreach ($param as $ko => $vo) {
                        // 键值
                        $key = preg_replace('/[^0-9]/', '', $vo);
                        // 键值内容
                        $value = str_replace($key, null, $vo);
                        //                        $columnValue[] = "{$key}={$value}";
                        $columnValue[] = [
                            'value' => $key,
                            'name' => $value,
                            'type' => $typeList[$ko],
                        ];
                        $columnValueList[] = $value;
                    }
                    $column['columnValue'] = $columnValue; //implode(',', $columnValue);
                    if ($val['COLUMN_NAME'] == 'status' || substr($val['COLUMN_NAME'], 0, 3) == 'is_') {
                        $column['columnSwitch'] = true;
                        $column['columnSwitchValue'] = implode('|', $columnValueList);
                        if ($val['COLUMN_NAME'] == 'status') {
                            $column['columnSwitchName'] = 'status';
                        } else {
                            $column['columnSwitchName'] = 'set' . str_replace(' ', null, ucwords(strtolower(str_replace('_', ' ', $val['COLUMN_NAME']))));
                        }
                    } else {
                        $column['columnSwitch'] = false;
                    }
                } else {
                    $column['columnComment'] = $val['COLUMN_COMMENT'];
                }
                $fields[] = $column;
            }
        }

        return $fields;
    }
}
