<?php

namespace App\Http\Controllers\Admin\v1;

use App\Helpers\Captcha;
use App\Http\Controllers\Controller;
use App\Models\Sys\SysConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Jiannei\Response\Laravel\Support\Facades\Response;

/**
 * 公共管理
 * SysmenuController class
 */
class CommonController extends Controller
{
    /**
     * 版本号
     * sysVar function
     *
     * @return void
     */
    public function sysVar()
    {
        $info = SysConfig::where('key', 'copyrightNumber')->first();

        return Response::success(['version' => $info->value]);
    }

    /**
     * 获取管理端配置
     * sysVar function
     *
     * @return void
     */
    public function sysSetting()
    {
        $returnData = [];
        $setting = SysConfig::where('type', 1)->get();
        foreach ($setting as $item) {
            // 系统配置
            if ($item->type == 1) {
                if ($item->key == 'login') {
                    $returnData[$item->key] = boolval($item->value);
                } else {
                    $returnData[$item->key] = $item->value;
                }
            }
        }

        return Response::success($returnData);
    }

    /**
     * 生成验证码
     *
     * @return void
     */
    public function captchaImg(Request $request)
    {
        // 实例化
        $captcha = new Captcha();
        // 输出验证码图片
        $captcha->doimg();
        // 获取验证码
        $code = $captcha->getCode();
        Cache::put('NumberVerificationCode_'.$request->input('_'), $code, 300);
        $captcha->outPut();
    }

    /**
     * 验证 验证码
     *
     * @return void
     */
    public static function verifyCaptcha($code, $time)
    {
        $CacheCoede = Cache::get('NumberVerificationCode_'.$time);
        Cache::forget('NumberVerificationCode_'.$time);

        return strtolower($CacheCoede) == strtolower($code) ? true : false;
    }
}
