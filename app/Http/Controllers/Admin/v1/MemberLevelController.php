<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Requests\MemberLevelRequest;
use App\Models\MemberLevelModel;

/**
 * 会员等级管理-控制器
 *
 * @author zhx
 *
 * @since: 2024/02/06
 * Class MemberLevelController
 */
class MemberLevelController extends BaseController
{
    /**
     * 构造函数
     *
     * @param  Request  $request
     *
     * @since 2024/02/06
     * MemberLevelController constructor.
     *
     * @author zhx
     */
    public function __construct()
    {
        $this->modelClass = new MemberLevelModel();
        $this->requestClass = MemberLevelRequest::class;
        $this->title = '会员等级';
        $this->initialize();
    }
}
