<?php

namespace App\Http\Controllers\Admin\v1;

use App\Helpers\Tree;
use App\Http\Requests\SysDicTypeRequest;
use App\Models\Sys\SysDicType;
use Illuminate\Http\Request;
use Jiannei\Response\Laravel\Support\Facades\Response;

/**
 * 数据字典分类管理
 * SysDicTypeController class
 */
class SysDicTypeController extends BaseController
{
    public function __construct()
    {
        $this->modelClass = new SysDicType();
        $this->requestClass = SysDicTypeRequest::class;
        $this->title = '数据字典分类';
        $this->initialize();
    }

    /**
     *  获取字典类型树
     *
     * @return void
     */
    public function tree(Request $request)
    {
        $list = $this->modelClass::whereNull('deleted_at')->get();
        Tree::init($list, 'parent_id', 'id', 'code');

        return Response::success(Tree::get_childall_data());
    }
}
