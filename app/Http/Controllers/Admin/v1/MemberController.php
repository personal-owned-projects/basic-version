<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Requests\MemberRequest;
use App\Models\MemberLevelModel;
use App\Models\MemberModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Jiannei\Response\Laravel\Support\Facades\Response;

/**
 * 会员管理-控制器
 *
 * @author zhx
 *
 * @since: 2024/02/06
 * Class MemberController
 */
class MemberController extends BaseController
{
    /**
     * 构造函数
     *
     * @param  Request  $request
     *
     * @since 2024/02/06
     * MemberController constructor.
     *
     * @author zhx
     */
    public function __construct()
    {
        $this->modelClass = new MemberModel();
        $this->requestClass = MemberRequest::class;
        $this->title = '会员';
        $this->initialize();
    }

    /**
     * 列表
     * index function
     *
     * @desc Display a listing of the resource.
     * @desc index： GET /re
     *
     * @param paging 是否分页，默认true
     * @param limit 显示条数
     * @param page 当前页
     * @param sort 排序参数
     * @param sortOrder 排序方式
     * @return void
     */
    public function index(Request $request)
    {
        $requestData = $request->all();
        [$paging, $limit, $page, $sort, $sortOrder] = [
            isset($requestData['paging']) ? $requestData['paging'] : true,
            isset($requestData['limit']) ? $requestData['limit'] : 10,
            isset($requestData['page']) ? $requestData['page'] : 1,
            isset($requestData['sort']) ? $requestData['sort'] : 'id',
            isset($requestData['sortOrder']) ? $requestData['sortOrder'] : 'desc',
        ];
        $models = $this->modelClass::query();

        if (isset($requestData['keyword'])) {
            $models->orWhere('username', 'like', '%'.$requestData['keyword'].'%');
            $models->orWhere('phone', 'like', '%'.$requestData['keyword'].'%');
            $models->orWhere('membership_code', 'like', '%'.$requestData['keyword'].'%');
            $models->orWhere('nickname', 'like', '%'.$requestData['keyword'].'%');
            $models->orWhere('realname', 'like', '%'.$requestData['keyword'].'%');
        }
        if (isset($requestData['groupId'])) {
            $levelInfo = MemberLevelModel::where('id', $requestData['groupId'])->first();
            if ($levelInfo) {
                // 下一条数据
                $nextInfo = MemberLevelModel::where('id', '>', $requestData['groupId'])->orderBy('id')->first();
                if ($nextInfo == null) {
                    $models->where('growth_value', '>=', $levelInfo->value);
                } else {
                    $models->where('growth_value', '>=', $levelInfo->value);
                    $models->where('growth_value', '<', $nextInfo->value);
                }
            }
        }

        // 是否分页
        if ((int) $paging == 1) {
            $list = $models->orderBy($sort, $sortOrder)->paginate($limit);

            return Response::success([
                'total' => $list->total(), // 总页数
                'page' => $list->currentPage(), // 当前页
                'pageSize' => $list->lastPage(), // 总页数
                'rows' => $list->items(), // 数据
            ]);
        } else {
            $list = $models->orderBy($sort, $sortOrder)->get();

            return Response::success($list);
        }
    }

    /**
     * 编辑保存
     * update function
     *
     * @desc Update the specified resource in storage.
     * @desc pdate： PUT /re/{re} 或 PATCH /re/{re}
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'state' => 'required',
        ], [
            'state.required' => '状态 不能为空。',
        ]);
        if ($validator->fails()) {
            return Response::fail($validator->errors()->first());
        }

        try {
            $models = $this->modelClass::query();
            $result = $models->where('id', $id)->update(['state' => $requestData['state']]);
            if ($result) {
                $this->logs($request, '200', '编辑', '编辑成功');

                return Response::ok('编辑成功');
            } else {
                $this->logs($request, '500', '编辑', '编辑失败');

                return Response::fail('编辑失败');
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return Response::fail('更新失败');
        }
    }
}
