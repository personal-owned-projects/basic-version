<?php

namespace App\Models\Sys;

use App\Models\Base;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SysDicData extends Base
{
    use HasFactory;

    protected $table = 'sys_dic_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'parent_id',
        'name',
        'key',
        'yx',
        'sort',
    ];
}
