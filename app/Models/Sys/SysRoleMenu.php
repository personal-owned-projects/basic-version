<?php

namespace App\Models\Sys;

use App\Models\Base;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SysRoleMenu extends Base
{
    use HasFactory;

    protected $table = 'sys_role_menu';
}
