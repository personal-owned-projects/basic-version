<?php

namespace App\Models\Sys;

use App\Models\Base;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SysMenuApi extends Base
{
    //    use HasFactory;

    protected $table = 'sys_menu_api';
}
