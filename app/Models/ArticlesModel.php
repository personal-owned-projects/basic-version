<?php

namespace App\Models;

/**
 * 文章-模型
 *
 * @author zhx
 *
 * @since 2024/01/20
 * Class ArticlesModel
 */
class ArticlesModel extends Base
{
    // 设置数据表
    protected $table = 'articles';

    // 字段
    protected $fillable = ['id', 'title', 'category_id', 'summary', 'cover_image', 'content', 'is_sell', 'sort', 'status'];

    /**
     * 关联分类
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hasCategories()
    {
        return $this->hasOne(CategoriesModel::class, 'id', 'category_id');
    }
}
