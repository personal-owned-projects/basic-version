<?php

namespace App\Models;

/**
 * 广告信息-模型
 *
 * @author zhx
 *
 * @since 2024/04/15
 * Class AdvertisementsModel
 */
class AdvertisementsModel extends Base
{
    // 设置数据表
    protected $table = 'advertisements';

    // 字段
    protected $fillable = ['id', 'category_id', 'title', 'image', 'type', 'link', 'store_page', 'sort', 'status'];

    /**
     * 关联分类
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hasCategories()
    {
        return $this->hasOne(CategoriesModel::class, 'id', 'category_id');
    }
}
