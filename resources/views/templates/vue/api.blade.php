import config from "@/config"
import http from "@/utils/request"

export default {
    list: {
        url: `${config.API_URL}/<?php echo $moduleName?>`,
        name: "获取<?php echo $moduleTitle?>列表",
        get: async function (params) {
            return await http.get(this.url, params);
        }
    },
    add: {
        url: `${config.API_URL}/<?php echo $moduleName?>`,
        name: "创建<?php echo $moduleTitle?>",
        post: async function (data = {}) {
            return await http.post(this.url, data);
        }
    },
    edit: {
        url: `${config.API_URL}/<?php echo $moduleName?>`,
        name: "编辑<?php echo $moduleTitle?>",
        put: async function (data = {}) {
            return await http.put(this.url + '/' + data.id, data);
        }
    },
    delete: {
        url: `${config.API_URL}/<?php echo $moduleName?>`,
        name: "删除<?php echo $moduleTitle?>",
        delete: async function (ids) {
            return await http.delete(this.url + '/' + ids);
        }
    }
}
