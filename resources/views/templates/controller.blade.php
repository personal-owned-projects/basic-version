
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers\Admin;

use App\Http\Requests\<?php echo $moduleName?>Request;
use App\Models\<?php echo $moduleName?>Model;

/**
 * <?php echo $moduleTitle?>管理-控制器
 * @author <?php echo $author?>

 * @since: <?php echo $since?>

 * Class <?php echo $moduleName?>Controller
 * @package App\Http\Controllers
 */
class <?php echo $moduleName?>Controller extends BaseController
{
    /**
     * 构造函数
     * @param Request $request
     * @since <?php echo $since?>

     * <?php echo $moduleName?>Controller constructor.
     * @author <?php echo $author?>

     */
    public function __construct()
    {
        $this->modelClass = new <?php echo $moduleName?>Model();
        $this->requestClass = <?php echo $moduleName?>Request::class;
        $this->title = '<?php echo $moduleTitle?>';
        $this->initialize();
    }

}
