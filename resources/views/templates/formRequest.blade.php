
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * <?php echo $moduleTitle?>-FormRequest
 * @author <?php echo $author?>

 * @since <?php echo $since?>

 * Class <?php echo $moduleName?>Model
 * @package App\Http\Requests
 */
class <?php echo $moduleName?>Request extends FormRequest
{

   /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

   /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
        <?php foreach ($columnList as $key => $item){ ?>
<?php if($item['columnName'] == 'id'){?>
    <?php echo "'{$item['columnName']}' => '',\n"?>
    <?php }else{?>
        <?php echo "'{$item['columnName']}' => 'required',\n"?>
    <?php }?>
<?php }?>
        ];
    }

   /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        // 验证提示语
        return [
        <?php foreach ($columnList as $key => $item){ ?>
        <?php echo "'{$item['columnName']}.required' => '{$item['columnComment']} 不能为空。',\n"?>
        <?php }?>
        ];
    }
}
